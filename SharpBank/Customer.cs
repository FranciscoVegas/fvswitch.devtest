﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace SharpBank
{
    public class Customer
    {
        private String name;
        private List<Account> accounts;

        public Customer(String name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        public String GetName()
        {
            return name;
        }

        public Customer OpenAccount(Account account)
        {
            accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned()
        {
            double total = 0;
            foreach (Account a in accounts)
                total += a.InterestEarned();
            return total;
        }

        public void transferBetweenAccounts(int fromTypeAccount, int toTypeAccount, double value)
        {
            try{
                validationTrasfer(fromTypeAccount, toTypeAccount, value);

                foreach (Account cta in accounts){
                    if (cta.GetAccountType()==fromTypeAccount){

                        cta.Withdraw(value);
                    }
                    if(cta.GetAccountType()==toTypeAccount){
                        cta.Deposit(value);
                    }

                }
            }catch (Exception ex ){
                throw ex;
            }


        }

        private void validationTrasfer(int fromTypeAccount, int toTypeAccount, double value){

            bool isPossibleTo = false;
            foreach (Account cta in accounts)
            {
                if (cta.GetAccountType() == fromTypeAccount && cta.SumTransactions() < value)
                {
                        throw new ArgumentException("amount must not be greater than total of account from");
                    
                }
                if (cta.GetAccountType() == toTypeAccount) { isPossibleTo = true; }

            }
            if(!isPossibleTo){
                throw new ArgumentException("no target account exists");
            }

        }
        /*******************************
         * This method gets a statement
         *********************************/
        public String GetStatement()
        {
            //JIRA-123 Change by Joe Bloggs 29/7/1988 start
            String statement = null; //reset statement to null here
            //JIRA-123 Change by Joe Bloggs 29/7/1988 end
            statement = "Statement for " + name + "\n";
            double total = 0.0;
            foreach (Account a in accounts)
            {
                statement += "\n" + StatementForAccount(a) + "\n";
                total += a.SumTransactions();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }

        private String StatementForAccount(Account a)
        {
            String s = "";

            //Translate to pretty account type
            switch (a.GetAccountType())
            {
                case Account.CHECKING:
                    s += "Checking Account\n";
                    break;
                case Account.SAVINGS:
                    s += "Savings Account\n";
                    break;
                case Account.MAXI_SAVINGS:
                    s += "Maxi Savings Account\n";
                    break;
            }

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction t in a.transactions)
            {
                s += "  " + (t.amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t.amount) + "\n";
                total += t.amount;
            }
            s += "Total " + ToDollars(total);
            return s;
        }

        private String ToDollars(double d)
        {
            return String.Format(CultureInfo.InvariantCulture,
                                   "${0:0,0.00}", Math.Abs(d));
        }
    }
}
