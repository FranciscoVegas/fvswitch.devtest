﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public  double amount;

        public DateTime transactionDate;

        public Transaction(double amountNew, bool withdraw=false)
        {
            if (withdraw){
                this.amount -= amountNew;    
            }else{
                this.amount += amountNew; 
            }

            this.transactionDate = DateProvider.GetInstance().Now();
        }

    }
}
